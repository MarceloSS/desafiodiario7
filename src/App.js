import { Component } from 'react';
import './App.css';
import Form from './Form';

class App extends Component {
  state = {
    customers: [],
    opened: false,
    draft: "",
  };

  toggleOpened = () => {
    // TODO: deve setar o state opened para true, caso esteja falso e vice-versa

    this.setState( { opened: !this.state.opened } );
  };

  addDraft = (draft) => {
    // TODO: setar o state draft, com o valor recebido por param

    this.setState( { draft} )
  };

  addCustomer = (customer) => {
    // TODO: adicionar o customer recebido por param no state customers. 
    // Também setar o draft para string vazia

    const { customers  } = this.state;
    this.setState( { customers: [...customers, customer ], draft: "" } );
  };

  render(){
    const { customers, opened, draft } = this.state;
    
    return (
      <div className="App">
        <header className="App-header">
          {customers.map((customer, index) => <div key={index}>{customer}</div>)}
          {!opened ? <button onClick={this.toggleOpened}>Novo</button> : <Form 
            onChange={this.props.onChange} 
            draft={draft} 
            addDraft={this.addDraft} 
            addCustomer={this.addCustomer} 
            toggleOpened={this.toggleOpened} />         
          }
        </header>
      </div>
    );
  }
}

export default App;
