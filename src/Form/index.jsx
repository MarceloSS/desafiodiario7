import { Component } from 'react';

class Form extends Component {
    state = {
        name: this.props.draft, // Componente deve receber essa prop chamada draft. Não esquece
    };

    setName = (event) => {
        
        this.setState( { name: event.target.value } );
    }

    componentWillUnmount() {
        // Executar o método, recebido por prop, addDraft e passar o valor state "name" por parâmetro.
        
        this.props.addDraft( this.state.name );
    }
    
    handleAddUser = () => {
        // Executar o método, recebido por prop, addCustomer e passar o valor state "name" por parâmetro.
       
        this.props.addCustomer( this.state.name );

       this.setState( { name: "" } );
    };

    render () {
        const { name } = this.state

        return(
            <span>
                <input onChange={this.setName} value={name}/>
                <button onClick={this.handleAddUser}>Salvar</button>
                <button onClick={this.props.toggleOpened}>Fechar</button>
            </span>
        )
    }
}

export default Form;